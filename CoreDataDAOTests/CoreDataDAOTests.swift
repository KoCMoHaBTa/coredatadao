//
//  CoreDataDAOTests.swift
//  CoreDataDAOTests
//
//  Created by Milen Halachev on 9/1/14.
//  Copyright (c) 2014 KoCMoHaBTa. All rights reserved.
//

import UIKit
import XCTest
import CoreDataDAO

class CoreDataDAOTests: XCTestCase {
    
    let bundle = Bundle(for: CoreDataDAOTests.self)
    
    override func setUp() {
        
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        super.tearDown()
    }
    
    func testSQLiteCoreDataStack() {
        
        SQLiteCoreDataStack.registerCoreDataStackType("CoreDataDAOTests")
        
        let preStackTypeCheck = CoreDataStack.coreDataStackType("CoreDataDAOTests") === SQLiteCoreDataStack.self
        let preStackInstanceCheck = (try! type(of: CoreDataStack.sharedInstance("CoreDataDAOTests", bundle: bundle)) as AnyClass) === (SQLiteCoreDataStack.self as AnyClass)
        
        XCTAssert(preStackTypeCheck, "CoreDataStackType is not SQLiteCoreDataStack")
        XCTAssert(preStackInstanceCheck, "CoreDataStack instance is not SQLiteCoreDataStack")
        
        
        
        
        self.performDBTest()
    
        
        
        
        CoreDataStack.removeCoreDataStackType("CoreDataDAOTests")
        
        let postStackTypeCheck = CoreDataStack.coreDataStackType("CoreDataDAOTests") === CoreDataStack.self
        let postStackInstanceCheck = (try! type(of: CoreDataStack.sharedInstance("CoreDataDAOTests", bundle: bundle)) as AnyClass) === (CoreDataStack.self as AnyClass)
        
        XCTAssert(postStackTypeCheck, "CoreDataStackType is not CoreDataStack")
        XCTAssert(postStackInstanceCheck, "CoreDataStack instance is not CoreDataStack")
        
        CoreDataStack.removeCoreDataStackType("CoreDataDAOTests")
    }
    
    func testInMemoryCoreDataStack() {
        
        InMemoryCoreDataStack.registerCoreDataStackType("CoreDataDAOTests")
        
        let preStackTypeCheck = CoreDataStack.coreDataStackType("CoreDataDAOTests") === InMemoryCoreDataStack.self
        let preStackInstanceCheck = (try! type(of: CoreDataStack.sharedInstance("CoreDataDAOTests", bundle: bundle)) as AnyClass) === (InMemoryCoreDataStack.self as AnyClass)
        
        XCTAssert(preStackTypeCheck, "CoreDataStackType is not InMemoryCoreDataStack")
        XCTAssert(preStackInstanceCheck, "CoreDataStack instance is not InMemoryCoreDataStack")
        
        
        
        
        self.performDBTest()
        
        CoreDataStack.removeCoreDataStackType("CoreDataDAOTests")
        
        let postStackTypeCheck = CoreDataStack.coreDataStackType("CoreDataDAOTests") === CoreDataStack.self
        let postStackInstanceCheck = (try! type(of: CoreDataStack.sharedInstance("CoreDataDAOTests", bundle: bundle)) as AnyClass) === (CoreDataStack.self as AnyClass)
        
        XCTAssert(postStackTypeCheck, "CoreDataStackType is not CoreDataStack")
        XCTAssert(postStackInstanceCheck, "CoreDataStack instance is not CoreDataStack")
        
        CoreDataStack.removeCoreDataStackType("CoreDataDAOTests")
    }
    
    func performDBTest() {
        
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
        
        XCTAssert(DAO(DAOPerson.self).isPersonDAO(), "This DAO is not instance of DAOPerson")
        
        //MARK: Test Schema Changes - requires creating factory and dao all the time in order to work properly
        
        let personDAO = DAO(DAOPerson.self)
        let companyDAO = DAO(Company.self)
        
        do { try personDAO.deleteAll() } catch { XCTFail("Delete failed: \(error)") }
        do { try companyDAO.deleteAll() } catch { XCTFail("Delete failed: \(error)") }
        
        XCTAssert(personDAO.fetchAll().count == 0, "We expect to have no persons at the beginig")
        XCTAssert(companyDAO.fetchAll().count == 0, "We expect to have no companies at the beginig")
        
        let p1 = personDAO.createObject()
        let p2 = personDAO.createObject()
        
        p1.firstName = "Genadi"
        p1.lastName = "Gredoredov"
        p1.age = 27
        
        p2.firstName = "Kuncho"
        p2.lastName = "Timelkov"
        p2.age = 17
        
        do { try personDAO.saveChanges() } catch { XCTFail("Save failed: \(error)") }
        
        let allPersons = personDAO.fetchAll()
        XCTAssert(allPersons.count ==  2, "We expect to have 2 persons")
        
        let adults = DAO(DAOPerson.self).fetchAdults()
        XCTAssert(adults.count == 1, "We expect to have 1 adult")
        
        let _ = DAO(Person.self).async({ let _ = $0.fetchAll() }) { (_) -> Void in
            
            XCTAssert(allPersons.count ==  2, "We expect to have 2 persons")
        }
    }
    
    func testPerformanceExample()  {
        
        // This is an example of a performance test case.
        self.measure() {
            
            // Put the code you want to measure the time of here.
        }
    }
    
}
