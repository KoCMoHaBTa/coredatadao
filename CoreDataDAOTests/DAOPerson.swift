//
//  DAOPerson.swift
//  CoreDataDAO
//
//  Created by Milen Halachev on 1/3/15.
//  Copyright (c) 2015 KoCMoHaBTa. All rights reserved.
//

import UIKit
import CoreData
import CoreDataDAO

class DAOPerson<E: Person>: DAOManagedObject<E> {
    
    required init(managedObjectModel: NSManagedObjectModel, managedObjectContext: NSManagedObjectContext) {
        
        super.init(managedObjectModel: managedObjectModel, managedObjectContext: managedObjectContext)
    }
    
    func isPersonDAO() -> Bool {
        
        print("This is really a Person DAO")
        return true
    }
    
    func fetchAdults() -> [E] {
        
        let predicate = NSPredicate(format: "%K > %@", "age", NSNumber(value: 18))
        let fetchRequest = self.fetchRequest(predicate)
        let adults = self.fetchBy(fetchRequest)
        
        return adults
    }
}
