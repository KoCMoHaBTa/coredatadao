//
//  EntityObserverTests.swift
//  CoreDataDAO
//
//  Created by Milen Halachev on 1/25/16.
//  Copyright © 2016 KoCMoHaBTa. All rights reserved.
//

import Foundation
import XCTest
import CoreDataDAO

class EntityObserverTests: XCTest {
    
    override func setUp() {
        
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        InMemoryCoreDataStack.registerCoreDataStackType("CoreDataDAOTests")
        
//        XCTAssert(inserted.count == 0, "Expected 0 inserted matching entityStateChange")
//        XCTAssert(updated.count == 0, "Expected 0 updated matching entityStateChange")
//        XCTAssert(deleted.count == 0, "Expected 0 deleted matching entityStateChange")
        
//        //person observers
//        
//        var observerAllPerson: EntityObserver<Person>! = EntityObserverCreate(Person).observe(changes: { (inserted, updated, deleted) -> Void in
//            
//            
//        })
//        
//        var observerInsertedPerson: EntityObserver<Person>! = EntityObserverCreate(Person).observe(entityStateChange: .Inserted, changes: { (inserted, updated, deleted) -> Void in
//            
//            XCTAssert(updated.count == 0, "Expected 0 updated matching entityStateChange")
//            XCTAssert(deleted.count == 0, "Expected 0 deleted matching entityStateChange")
//        })
//        
//        var observerUpdatedPerson: EntityObserver<Person>! = EntityObserverCreate(Person).observe(entityStateChange: .Updated, changes: { (inserted, updated, deleted) -> Void in
//            
//            XCTAssert(inserted.count == 0, "Expected 0 inserted matching entityStateChange")
//            XCTAssert(deleted.count == 0, "Expected 0 deleted matching entityStateChange")
//        })
//        
//        var observerDeletedPerson: EntityObserver<Person>! = EntityObserverCreate(Person).observe(entityStateChange: .Deleted, changes: { (inserted, updated, deleted) -> Void in
//            
//            XCTAssert(inserted.count == 0, "Expected 0 inserted matching entityStateChange")
//            XCTAssert(updated.count == 0, "Expected 0 updated matching entityStateChange")
//        })
//        
//        //company observers
//        
//        var observerAllCompany: EntityObserver<Company>! = EntityObserverCreate(Company).observe(changes: { (inserted, updated, deleted) -> Void in
//            
//            
//        })
//        
//        var observerInsertedCompany: EntityObserver<Company>! = EntityObserverCreate(Company).observe(entityStateChange: .Inserted, changes: { (inserted, updated, deleted) -> Void in
//            
//            XCTAssert(updated.count == 0, "Expected 0 updated matching entityStateChange")
//            XCTAssert(deleted.count == 0, "Expected 0 deleted matching entityStateChange")
//        })
//        
//        var observerUpdatedCompany: EntityObserver<Company>! = EntityObserverCreate(Company).observe(entityStateChange: .Updated, changes: { (inserted, updated, deleted) -> Void in
//            
//            XCTAssert(inserted.count == 0, "Expected 0 inserted matching entityStateChange")
//            XCTAssert(deleted.count == 0, "Expected 0 deleted matching entityStateChange")
//        })
//        
//        var observerDeletedCompany: EntityObserver<Company>! = EntityObserverCreate(Company).observe(entityStateChange: .Deleted, changes: { (inserted, updated, deleted) -> Void in
//            
//            XCTAssert(inserted.count == 0, "Expected 0 inserted matching entityStateChange")
//            XCTAssert(updated.count == 0, "Expected 0 updated matching entityStateChange")
//        })
        
    }
    
    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        super.tearDown()
        
        CoreDataStack.removeCoreDataStackType("CoreDataDAOTests")
    }
    
    func testInsert() {
        
    }
    
    func testUpdate() {
        
    }
    
    func testDelete() {
        
    }
    
    func testFilter() {
        
//        var observerWithFilter: EntityObserver<Person>! = EntityObserverCreate(Person).observe(entityStateChange: .Updated, filter: { $0.age == 28 }, changes: { (inserted, updated, deleted) -> Void in
//            
//            XCTAssert(inserted.count == 0, "Expected 0 inserted matching entityStateChange")
//            XCTAssert(deleted.count == 0, "Expected 0 deleted matching entityStateChange")
//            
//            XCTAssert(updated.count == 1, "Only one update record matching this filter is expected")
//            
//            
//        })
//        
//        observerWithFilter = nil
    }
    
}