#Deprecated

This project is deprecated in favor of [MHCoreDataKit](https://github.com/KoCMoHaBTa/MHCoreDataKit).
The current state of the project is updated for Swift 4.0 using Xcode 9.0.
Future changes are very unlikely to uccur.

#Migration guide to MHCoreDataKit

**MHCoreDataKit** is extension and protocol based framework on top of CoreData that provides similar functionalities as **MHCoreDataKit**.

Most of the functionalities of **CoreDataDAO** are pored as extensions of native CoreData objects. Consider migrating your code to use the extension based functionalities of **MHCoreDataKit** in order to reduce unnececary complexity within your app.

`CoreDataStack` - in **MHCoreDataKit** a core data stack is represented as a simple protocol. There is default implementation and `NSPersistentContainer` is made to conform to that protocol. However the automatic lookup functionality does not exist, since almost the same functionality was intoroduced as of **iOS 10**.

`DAOManagedObject` - in **MHCoreDataKit**, all of the DAO behaviour was intoduced as extension of `NSFetchRequest`. You will find various configuration methos of `NSFetchRequest` that would allow it to perform exactly the same as `DAOManagedObject`.

`EntityLookup` - these extensions are transferred and enchansed by **MHCoreDataKit**. They  appear as standalone extensions of thier respective objects

`EntityObserver` - it was directly transferred with mionor enchansed by **MHCoreDataKit**.

CoreDataDAO   		| MHCoreDataKit
------------- 		| -------------
CoreDataStack 		| CoreDataStack, DefaultCoreDataStack, NSPersistentContainer
DAOManagedObject  	| NSFetchRequest
EntityLookup  		| Extensions of CoreData objects
EntityLookup  		| EntityLookup

#CoreDataDAO

CoreDataDAO is a swift framework that tries to hide the whole complexity of CoreData behind more known and easier to use interface - a DAO pattern.

##Files

- **CoreDataStack** - core data stack abstract implementation
- **SQLiteCoreDataStack** - core data stack implementation for NSSQLiteStoreType
- **InMemoryCoreDataStack** - core data stack implementation for NSInMemoryStoreType
- **DAOManagedObject** - implemnts DAO interface for Entity management
- **DAOManagedObjectProtocol** - helper protocol, used for generic function parameter
- **EntityLookup** - contains bunch of extensions and utilities
- **DAO** - the magic functions that you will use the most

##Configuration

The default configuration for **SQLiteCoreDataStack** is the following: 

- Prsistent stores directory: **`<Library>/CoreDataStores/<ModelName>/<ModelName>.sqlite`**
- Persistent Store Type: **NSSQLiteStoreType**
- NSPersistentStoreCoordinator options: **[NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]**

However you can subclass **CoreDataStack** and override its configuration methods.    
In order for your subclass to work, you must register it using **registerCoreDataStackType**
function.

##How to install  

Well probably the most clean options here are to:  

- copy the source to your project  
- download the project and link it as dependency to your project **(Preferable)**

##How to use

- Setup your core data model and entites.
- In your core data model, make sure that the class of all entities is prefixed with their module name, like **`<ModuleName>.<EntityName>`** or **CoreDataDAOTests.Person**
- if linked, import the framework, like **`import CoreDataDAO`**
- register the desired **(stack type - model name)** pair, like **SQLiteCoreDataStack.registerCoreDataStackType("ModelName")**
- just call the DAO function and pass the entity type as argument, like **`DAO(<Entity>)`** or **DAO(Person)**
- get all persons - **DAO(Person).fetchAll()**

##Limitations

- **You can only load a single set of Entities, Model and Stack Type**. This means that you cannot load the **Person** entity in multiple models or to load a single model in multiple stack types, like having both SQLite and InMemory version of the same model. 

##Support

Well there is no perfect software, so if you find any issues - please report them using the issue tracking within the repository.

##Changelog

###1.5.0

- updated for swift 4.0

###1.4.0

- updated for swift 3.0

###1.3.2

- updated for swift 2.2

###1.3.1

- added support for NSAsynchronousFetchRequest and implicit async fetching

###1.3.0

- added EntityObserver that can be used to observe changes in CoreData

###1.2.1

- made scheme shared

###1.2.0

- updated for swift 2.0
- updated code syntax formatting
- replcaed all NSException raising with try! exceptions

###1.1.0

- updated for swift 1.2
- removed NSObjectProtocol conformance and NSObject inheritance

- CoreDataStack:
	- refactored to abstract from persistentStoreType and other configurations
	- renamed databaseName to modelName
	- added will and did deleteDB(s) methods and made the deleteDB(s) methods final
	- removing stack type now destroys the current shared instance
	- added storeURL as optional configuration method and optional parameter when creating NSPersistentStoreCoordinator
	- registration of stack type per model name is now required - an exception is thrown if the default abstract stack is loaded	

- added SQLiteCoreDataStack and InMemoryCoreDataStack that represents the respective store types
- renamed all "database\*" parameters to "model\*"

###1.0.0

- Initial Version





##License

BSD License

Copyright (c) 2014, Milen Halachev
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
must display the following acknowledgement:
This product includes software developed by Milen Halachev.
4. Neither the name of Milen Halachev nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MILEN HALACHEV ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MILEN HALACHEV BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
