/*

Copyright (c) 2014, Milen Halachev
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
must display the following acknowledgement:
This product includes software developed by Milen Halachev.
4. Neither the name of Milen Halachev nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MILEN HALACHEV ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MILEN HALACHEV BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

import Foundation
import CoreData

extension NSManagedObjectModel {
    
    public var entitiesByClassName: [String: NSEntityDescription] {
        
       get {
        
            let entities = self.entities 
            var entitiesByClassName: [String: NSEntityDescription] = Dictionary<String, NSEntityDescription>()
        
            for entity in entities {
                
                entitiesByClassName[entity.managedObjectClassName] = entity
            }
        
            return entitiesByClassName
        }
    }
}

extension NSEntityDescription {
    
    public class func entityForClassName(_ entityClassName: String, inManagedObjectContext context: NSManagedObjectContext) -> NSEntityDescription? {
        
        if let model = context.persistentStoreCoordinator?.managedObjectModel {
            
            return model.entitiesByClassName[entityClassName]
        }
        
        return nil
    }
}

extension NSManagedObject {
    
    public class func modelName() -> String? {
        
        return nil
    }
    
    public class func modelBundle() -> Bundle {
        
        return Bundle(for: self)
    }
    
    public class func coreDataStack() -> CoreDataStack {
        
        let coreDataStack = try! self.lookupCoreDataStack()
        
        return coreDataStack
    }
    
    class func lookupCoreDataStack() throws -> CoreDataStack {
        
        var modelName = self.modelName()
        var modelBundle = self.modelBundle()
        
        if modelName == nil {
            
            let (lookupmodelName, containerBundle) = self.lookupModelName(inBundle: modelBundle, recursively: true)
            
            modelName = lookupmodelName
            
            if let containerBundle = containerBundle {
                
                modelBundle = containerBundle
            }
        }
        
        if modelName == nil {
            
            throw CoreDataDAOError.general("Unable to find modelName for entity \(self)")
        }
        
        let stack = try CoreDataStack.sharedInstance(modelName!, bundle: modelBundle)
        return stack
    }
    
    class func lookupModelName(inBundle bundle: Bundle, recursively: Bool) -> (modelName: String?, containerBundle: Bundle?) {
        
        struct EntityModelNamePairs {
            
            //this dictionary holds all looked up classes as keys and their modelName and bundle as values
            static var pairs: Dictionary<String, Dictionary<String, AnyObject>> = Dictionary()
        }
        
        let className = NSStringFromClass(self)
        
        //check if we allready have bundle and modelName for this class
        if let modelName = EntityModelNamePairs.pairs[className]?["modelName"] as? String {
            
            let containerBundle: Bundle? = EntityModelNamePairs.pairs[className]?["modelName"] as? Bundle
            
            return (modelName, containerBundle)
        }
        
        //if we have dbName in provided bundle - return the result
        if let modelName = self.lookupModelName(inBundle: bundle) {
            
            EntityModelNamePairs.pairs[className] = ["modelName": modelName as AnyObject, "bundle": bundle]
            return (modelName, bundle)
        }
        
        //if we are here - there is non model containing our class
        //if recursively is YES - get all bundles contained within the provided one and lookup them recursively
        if recursively {
            
            if let subBundles = bundle.urls(forResourcesWithExtension: "bundle", subdirectory: nil) {
                
                for childBundleURL in subBundles {
                    
                    if let childBundle = Bundle(url: childBundleURL) {
                        
                        let (modelName, _) = self.lookupModelName(inBundle: childBundle, recursively: recursively)
                        
                        if let modelName = modelName {
                            
                            EntityModelNamePairs.pairs[className] = ["modelName": modelName as AnyObject, "bundle": childBundle]
                            return (modelName, childBundle)
                        }
                    }
                }
            }
        }
        
        //if we come here - there is no model that contains our class
        return (nil, nil)
    }
    
    class func lookupModelName(inBundle bundle: Bundle) -> String? {
        
        let className = NSStringFromClass(self)
        
        //start looking up - first get all models for the provided bundle
        if let modelURLs = bundle.urls(forResourcesWithExtension: "momd", subdirectory: nil) {
            
            //iterate over all models in order to find if any of them contains the required class name
            for url in modelURLs {
                
                if let model = NSManagedObjectModel(contentsOf: url) {
                    
                    let entities = model.entities
                    let matches = entities.filter {
                        
                        $0.managedObjectClassName == className
                    }
                    
                    assert(matches.count < 2, "More than one match")
                    
                    if matches.count > 0 {
                        
                        return url.deletingPathExtension().lastPathComponent
                    }
                }
            }
        }
        
        return nil
    }
}



