/*

Copyright (c) 2014, Milen Halachev
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
must display the following acknowledgement:
This product includes software developed by Milen Halachev.
4. Neither the name of Milen Halachev nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MILEN HALACHEV ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MILEN HALACHEV BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

import Foundation
import CoreData

open class DAOManagedObject<E:NSManagedObject>: DAOManagedObjectProtocol {
    
    public final fileprivate(set) var managedObjectModel: NSManagedObjectModel
    fileprivate var managedObjectContext: NSManagedObjectContext
    fileprivate var transactionContexts: Array<NSManagedObjectContext> = []
    
    public final var currentContext: NSManagedObjectContext {
        
        return self.transactionContexts.last ?? self.managedObjectContext
    }
    
    open fileprivate(set) var error: Error?
    
    // MARK: - Async
    
    fileprivate var async: Bool = false
    fileprivate var asyncResult: NSAsynchronousFetchResult<E>!
    fileprivate var asyncCompletionBlock: ((NSAsynchronousFetchResult<E>) -> Void)!
    
    //MARK: - DAOManagedObjectProtocol
    
    public typealias EntityType = E
    
    public required init(managedObjectModel: NSManagedObjectModel, managedObjectContext: NSManagedObjectContext) {
        
        self.managedObjectModel = managedObjectModel
        self.managedObjectContext = managedObjectContext
    }
    
    //MARK: - Concurrency
    
    open func performBlock(_ block: @escaping (_ dao: DAOManagedObject) -> Void) {
        
        self.currentContext.perform { () -> Void in
            
            block(self);
        }
    }
    
    open func performBlockAndWait(_ block: @escaping (_ dao: DAOManagedObject) -> Void) {
        
        self.currentContext.performAndWait { () -> Void in
            
            block(self);
        }
    }
    
    //MARK: - Transactions
    
    open func beginTransaction() {
        
        let concurrecnyType: NSManagedObjectContextConcurrencyType = Thread.isMainThread ? .mainQueueConcurrencyType : .privateQueueConcurrencyType
        let transactionContext: NSManagedObjectContext = NSManagedObjectContext(concurrencyType: concurrecnyType)
        transactionContext.parent = self.currentContext
        self.transactionContexts.append(transactionContext)
    }
    
    open func endTransaction() {
        
        self.transactionContexts.removeLast()
    }
    
    //MARK: - Single Object Management
    
    open func createObject() -> E {
        
        return try! self.createAndInsertManagedObject(E.self, model: self.managedObjectModel, context: self.currentContext)
    }
    
    open func createTemporaryObject() -> E {
        
        return try! self.createManagedObject(E.self, model: self.managedObjectModel)
    }
    
    open func insertTemporaryObject(_ object: E)
    {
        if object.managedObjectContext == nil
        {
            self.insertManagedObject(object, context: self.currentContext)
        }
    }
    
    open func deleteObject(_ object: E) {
        
        self.currentContext.delete(object)
    }
    
    //MARK: - Multiple Objects Management
    
    open func fetchAll() -> [E] {
        
        return self.fetchBy(nil)
    }
    
    open func fetchAll(_ sortKey: String, ascending: Bool) -> [E] {
        
        return self.fetchBy(nil, sortDescriptor: NSSortDescriptor(key: sortKey, ascending: ascending))
    }
    
    open func fetchAll(_ sortDescriptors: [NSSortDescriptor]?) -> [E] {
        
        return self.fetchBy(nil, sortDescriptors: sortDescriptors)
    }
    
    open func countAll() -> Int {
        
        return self.countBy(self.fetchRequest(nil))
    }

    open func deleteAll() throws {
        
        let all = self.fetchAll()
        
        for object in all {
            
            self.currentContext.delete(object)
        }
        
        try self.saveChanges()
    }
    
    open func saveChanges() throws {
        
        var context: NSManagedObjectContext? = self.currentContext
        
        while context != nil {
            
            try context!.performBlockAndWait(block: { () -> Void in
                
                try context!.save()
            })
            
            context = context!.parent
        }
    }
    
    //MARK: - Key-Value Fetching
    
    open func fetchBy(_ key: String, value: AnyObject) -> [E] {
        
        let predicateFormat = "%K == %@"
        let predicate = NSPredicate(format: predicateFormat, argumentArray: [key, value])
        let fetchRequest = self.fetchRequest(predicate)
        
        return self.fetchBy(fetchRequest)
    }
    
    open func fetchBy(_ key: String, value: AnyObject, sortKey: String, ascending: Bool) -> [E] {
        
        let predicateFormat = "%K == %@"
        let predicate = NSPredicate(format: predicateFormat, argumentArray: [key, value])
        let sortDescriptor = NSSortDescriptor(key: sortKey, ascending: ascending)
        let fetchRequest = self.fetchRequest(predicate, sortDescriptor: sortDescriptor)
        
        return self.fetchBy(fetchRequest)
    }
    
    open func fetchBy(_ key: String, values: [AnyObject]) -> [E] {
        
        let predicateFormat = "%K IN %@"
        let predicate = NSPredicate(format: predicateFormat, argumentArray: [key, values])
        let fetchRequest = self.fetchRequest(predicate)
        
        return self.fetchBy(fetchRequest)
    }
    
    open func fetchBy(_ key: String, values: [AnyObject], sortKey: String, ascending: Bool) -> [E] {
        
        let predicateFormat = "%K IN %@"
        let predicate = NSPredicate(format: predicateFormat, argumentArray: [key, values])
        let sortDescriptor = NSSortDescriptor(key: sortKey, ascending: ascending)
        let fetchRequest = self.fetchRequest(predicate, sortDescriptor: sortDescriptor)
        
        return self.fetchBy(fetchRequest)
    }
    
    open func fetchBy(_ keyValues: [String: AnyObject]) -> [E] {
        
        var format: String?
        var arguments: [AnyObject] = []
        
        for (key, value) in keyValues {
            
            if format != nil {
                
                format = (format?.appendingFormat(" AND %@ ==", key))! + "%@"
            }
            else {
                
                format = String(format: "%@ == ", key) + "%@"
            }
            
            arguments.append(value)
        }
        
        let prepdicate = NSPredicate(format: format!, argumentArray: arguments)
        
        return self.fetchBy(prepdicate)
    }
    
    open func fetchBy(_ keyValues: [String: AnyObject], sortKey: String, ascending: Bool) -> [E] {
        
        var format: String?
        var arguments: [AnyObject] = []
        
        for (key, value) in keyValues {
            
            if format != nil {
                
                format = (format?.appendingFormat(" AND %@ ==", key))! + "%@"
            }
            else {
                
                format = String(format: "%@ == ", key) + "%@"
            }
            
            arguments.append(value)
        }
        
        let prepdicate = NSPredicate(format: format!, argumentArray: arguments)
        let sortDescriptor = NSSortDescriptor(key: sortKey, ascending: ascending)
        
        return self.fetchBy(prepdicate, sortDescriptor: sortDescriptor)
    }

//    -(id)fetchFirstForKey:(NSString*)key value:(id)value;
//    -(id)fetchFirstForKey:(NSString*)key value:(id)value sortedByKey:(NSString*)key ascending:(BOOL)ascending;
    
    //MARK: - Fetch with predicate
    
    open func fetchBy(_ predicate: NSPredicate?) -> [E] {
        
        return self.fetchBy(predicate, sortDescriptors: nil)
    }
    
    open func fetchBy(_ predicate: NSPredicate?, sortDescriptor: NSSortDescriptor) -> [E] {
        
        return self.fetchBy(predicate, sortDescriptors: [sortDescriptor])
    }
    
    open func fetchBy(_ predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?) -> [E] {
        
        let fetchRequest = self.fetchRequest(predicate, sortDescriptors: sortDescriptors)
        return self.fetchBy(fetchRequest)
    }
    
    //MARK: - Creating Fetch Requests
    
    open func fetchRequest(_ predicate: NSPredicate?) -> NSFetchRequest<E> {
        
        return self.fetchRequest(predicate, sortDescriptors: nil)
    }
    
    open func fetchRequest(_ predicate: NSPredicate?, sortDescriptor: NSSortDescriptor) -> NSFetchRequest<E> {
        
        return self.fetchRequest(predicate, sortDescriptors: [sortDescriptor])
    }
    
    open func fetchRequest(_ predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?) -> NSFetchRequest<E> {
        
        let context = self.currentContext;
        let entityClassName = NSStringFromClass(E.self);
        let entity = NSEntityDescription.entityForClassName(entityClassName, inManagedObjectContext: context)
        
        return self.fetchRequest(entity, predicate: predicate, sortDescriptors: sortDescriptors)
    }
    
    open func fetchRequest(_ entity: NSEntityDescription?, predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?) -> NSFetchRequest<E> {

        let fetchRequest = NSFetchRequest<E>()
        
        fetchRequest.entity = entity
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = sortDescriptors
        
        return fetchRequest;
    }
    
    //MARK: Fetch with Fetch Request
    
    open func fetchBy(_ fetchRequest: NSFetchRequest<E>) -> [E] {
        
        if self.async {
            
            let request = NSAsynchronousFetchRequest<E>(fetchRequest: fetchRequest, completionBlock: self.asyncCompletionBlock)
            self.asyncResult = self.fetchBy(request)
            return []
        }
        
        do {
            
            let result = try self.currentContext.fetch(fetchRequest)
            return result
        }
        catch let e {
            
            self.error = e
        }
        
        return []
    }
    
    open func countBy(_ fetchRequest: NSFetchRequest<E>) -> Int {
        
        do {
            
            let result = try self.currentContext.count(for: fetchRequest)
            return result
        }
        catch {
            
            self.error = error
            return NSNotFound
        }
    }
    
    //MARK: - Async Fetch Request
    
    open func fetchBy(_ request: NSAsynchronousFetchRequest<E>) -> NSAsynchronousFetchResult<E> {
        
        return try! self.currentContext.execute(request) as! NSAsynchronousFetchResult
    }
    
    open func async(_ fetch: (_ dao: DAOManagedObject<E>) -> Void, completion: @escaping (_ result: [E]) -> Void) -> NSAsynchronousFetchResult<E> {
        
        self.async = true
        self.asyncCompletionBlock = { (result: NSAsynchronousFetchResult) -> Void in
            
            defer {
                
                self.async = false
                self.asyncCompletionBlock = nil
                self.asyncResult = nil
            }
            
            completion(result.finalResult ?? [])
        }
        
        fetch(self)
        
        return self.asyncResult
    }
    
    //MARK: - Fetch by ObjectID
    
    open func fetchBy(_ objectID: NSManagedObjectID) -> E? {
        
        do {
            
            if let obj = try self.currentContext.existingObject(with: objectID) as? E {
                
                return obj
            }
        }
        catch let e {
            
            self.error = e
        }
        
        return nil
    }
    
    func fetchBy(_ objectIDURL: URL) -> E? {
        
        if let managedObjectID = self.currentContext.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: objectIDURL) {
            
            return self.fetchBy(managedObjectID)
        }
        
        return nil
    }
    
    //MARK: - NSManagedObject management
    
    fileprivate func createManagedObject(_ entityType: E.Type, model: NSManagedObjectModel) throws -> E {
        
        let entityClassName: String = NSStringFromClass(entityType)
        let context: NSManagedObjectContext? = nil
        
        guard let entityDescription = model.entitiesByClassName[entityClassName] else {
            
            throw CoreDataDAOError.general("Unable to find entity for class name: \(entityClassName)")
        }
        
        let result: E = NSManagedObject(entity: entityDescription, insertInto: context) as! E
        
        return result
    }
    
    fileprivate func insertManagedObject(_ managedObject: E, context: NSManagedObjectContext) {
        
        context.insert(managedObject)
    }
    
    fileprivate func createAndInsertManagedObject(_ entityType: E.Type, model: NSManagedObjectModel, context: NSManagedObjectContext) throws -> E {
        
        let result: E = try self.createManagedObject(entityType, model: model)
        self.insertManagedObject(result, context: context)
        return result
    }
}





