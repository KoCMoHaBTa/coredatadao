/*

Copyright (c) 2014, Milen Halachev
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
must display the following acknowledgement:
This product includes software developed by Milen Halachev.
4. Neither the name of Milen Halachev nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MILEN HALACHEV ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MILEN HALACHEV BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

import Foundation
import CoreData

open class CoreDataStack {
    
    public let modelURL: URL
    public let modelName: String
    
    //MARK: - Singleton Instances
    
    fileprivate struct Static {
        
        fileprivate static var stackTypes: Dictionary<String, CoreDataStack.Type> = Dictionary<String, CoreDataStack.Type>()
        fileprivate static var sharedInstances: Dictionary<String, CoreDataStack> = Dictionary<String, CoreDataStack>()
    }
    
    public final class func sharedInstance(_ modelName: String) throws -> CoreDataStack {
        
        return try self.sharedInstance(modelName, bundle: nil)
    }
    
    public final class func sharedInstance(_ modelName: String, bundle: Bundle?) throws -> CoreDataStack {
        
        guard let sharedInstance = Static.sharedInstances[modelName] else {
            
            let sharedInstance = try self.coreDataStackType(modelName).init(modelName: modelName, bundle: bundle)
            Static.sharedInstances[modelName] = sharedInstance
            
            return sharedInstance
        }
        
        return sharedInstance
    }
    
    //MARK: - Initializers by model URL
    
    public required init(modelURL: URL) {
        
        self.modelURL = modelURL
        self.modelName = modelURL.deletingPathExtension().lastPathComponent
        
        self.setupDB(self.modelName)
    }
    
    //MARK: - Initializers by model name
    
    public required init(modelName: String, bundle: Bundle?) throws {
        
        self.modelName = modelName
        
        //set bundle
        let bundle = bundle ?? Bundle.main
        
        guard let modelURL = bundle.url(forResource: self.modelName, withExtension: "momd") else {
            
            self.modelURL = URL(string: "")!
            throw CoreDataDAOError.general("Unable to find model named \(modelName)")
        }
        
        self.modelURL = modelURL
        
        self.setupDB(self.modelName)
    }
    
    public convenience init(modelName: String) throws {
        
        try self.init(modelName: modelName, bundle: nil)
    }
    
    //MARK: - Type Registration
    
    public final class func registerCoreDataStackType(_ coreDataStackType: CoreDataStack.Type, modelName: String) {
        
        Static.stackTypes[modelName] = coreDataStackType
    }
    
    public final class func registerCoreDataStackType(_ modelName: String) {
        
        self.registerCoreDataStackType(self, modelName: modelName)
    }
    
    public final class func removeCoreDataStackType(_ modelName: String) {
        
        Static.stackTypes[modelName] = nil
        
        //reset the core data stack
        Static.sharedInstances[modelName]?.resetCoreDataStack()
        
        //remove the shared instance
        Static.sharedInstances[modelName] = nil
    }
    
    public final class func coreDataStackType(_ modelName: String) -> CoreDataStack.Type {
        
        return Static.stackTypes[modelName] ?? self
    }
    
    
    
    
    
    //MARK: - Transaction CoreDataStack
    
//    private init(transactionCoreDataStack stack: CoreDataStack)
//    {
//        self.modelName = stack.modelName
//        self.modelURL = stack.modelURL
//        self.storeURL = stack.storeURL
//        
//        super.init()
//        
//        _managedObjectModel = stack.managedObjectModel
//        _persistentStoreCoordinator = stack.persistentStoreCoordinator
//        
//        _rootManagedObjectContext = stack.rootManagedObjectContext
//        _mainManagedObjectContext = stack.mainManagedObjectContext
//        _workerManagedObjectContext = nil //lazy create new one
//    }
//    
//    public func transactionCoreDataStack() -> CoreDataStack
//    {
//        let transactionCoreDataStack = CoreDataStack(transactionCoreDataStack: self)
//        
//        return transactionCoreDataStack
//    }
    
    //MARK: - Concurrency
    
    public final func performBlock(_ context: NSManagedObjectContext, block: @escaping (_ context: NSManagedObjectContext) -> Void)  {
        
        context.perform { () -> Void in
            
            block(context)
        }
    }
    
    public final func performBlock(_ context: NSManagedObjectContext, block: @escaping (_ context: NSManagedObjectContext) throws -> Void, errorHandler: @escaping (_ context: NSManagedObjectContext, _ error: Error) -> Void)  {
        
        context.performBlock({ () -> Void in
            
           try block(context)
            
        }) { (error) -> Void in
                
            errorHandler(context, error)
        }
    }
    
    public final func performBlockAndWait(context: NSManagedObjectContext, block: @escaping (_ context: NSManagedObjectContext) throws -> Void) rethrows {
        
        try context.performBlockAndWait { () -> Void in
            
            try block(context)
        }
    }
    
    //MARK: -
    
    public final func performBlock(_ block: @escaping (_ context: NSManagedObjectContext) -> Void) {
        
        self.performBlock(self.currentContext, block: block)
    }
    
    public final func performBlock(_ block: @escaping (_ context: NSManagedObjectContext) -> Void, errorHandler: @escaping (_ context: NSManagedObjectContext, _ error: Error) -> Void) {
        
        self.performBlock(self.currentContext, block: block, errorHandler: errorHandler)
    }
    
    public final func performBlockAndWait(block: @escaping (_ context: NSManagedObjectContext) throws -> Void) rethrows {
        
        try self.performBlockAndWait(context: self.currentContext, block: block)
    }
    
    //MARK: - Core Data Stack
    
    fileprivate var _managedObjectModel: NSManagedObjectModel?
    open fileprivate(set) var managedObjectModel: NSManagedObjectModel {
        
        get {
            
            if _managedObjectModel == nil {
                
                _managedObjectModel = self.createNewNSManagedObjectModel()
            }

            return _managedObjectModel!
        }
        
        set {
            
            _managedObjectModel = newValue
        }
    }
    
    fileprivate var _persistentStoreCoordinator: NSPersistentStoreCoordinator?
    open fileprivate(set) var persistentStoreCoordinator: NSPersistentStoreCoordinator {
        
        get {
            
            if _persistentStoreCoordinator == nil {
                
                _persistentStoreCoordinator = self.createNewNSPersistentStoreCoordinator()
            }
            
            return _persistentStoreCoordinator!
        }
        
        set {
            
            _persistentStoreCoordinator = newValue
        }
    }
    
    
    fileprivate var _rootManagedObjectContext: NSManagedObjectContext?
    open fileprivate(set) var rootManagedObjectContext: NSManagedObjectContext {
        
        get {
            
            if _rootManagedObjectContext == nil {
                
                _rootManagedObjectContext = self.createNewRootManagedObjectContext()
            }
            
            return _rootManagedObjectContext!
        }
        
        set {
            
            _rootManagedObjectContext = newValue
        }
    }
    
    fileprivate var _mainManagedObjectContext: NSManagedObjectContext?
    open fileprivate(set) var mainManagedObjectContext: NSManagedObjectContext {
        
        get {
            
            if _mainManagedObjectContext == nil {
                
                _mainManagedObjectContext = self.createNewMainManagedObjectContext()
            }
            
            return _mainManagedObjectContext!
        }
        
        set {
            
            _mainManagedObjectContext = newValue
        }
    }
    
    fileprivate var _backgroundManagedObjectContext: NSManagedObjectContext?
    open fileprivate(set) var backgroundManagedObjectContext: NSManagedObjectContext {
        
        get {
            
            if _backgroundManagedObjectContext == nil {
                
                _backgroundManagedObjectContext = self.createNewBackgroundManagedObjectContext()
            }
            
            return _backgroundManagedObjectContext!
        }
        
        set {
            
            _backgroundManagedObjectContext = newValue
        }
    }
    
    open var currentContext: NSManagedObjectContext {
        
        return Thread.isMainThread ? self.mainManagedObjectContext : self.backgroundManagedObjectContext
    }
    
    fileprivate func createNewRootManagedObjectContext() -> NSManagedObjectContext {
        
        let coordinator = self.persistentStoreCoordinator
        
        let context:NSManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.persistentStoreCoordinator = coordinator
        
        return context
    }
    
    fileprivate func createNewMainManagedObjectContext() -> NSManagedObjectContext {
        
        let rootContext = self.rootManagedObjectContext
        
        let context:NSManagedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.parent = rootContext
        
        return context
    }
    
    fileprivate func createNewBackgroundManagedObjectContext() -> NSManagedObjectContext {
        
        let mainContext = self.mainManagedObjectContext
        
        let context:NSManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.parent = mainContext
        
        return context
    }
    
    fileprivate func createNewNSManagedObjectModel() -> NSManagedObjectModel {
        
        let modelURL = self.modelURL
        
        let managedObjectModel = try! { () throws -> NSManagedObjectModel in
            
            guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
                
                throw CoreDataDAOError.general("Unable to find model at URL: \(modelURL)")
            }
            
            return managedObjectModel
        }()
        
        return managedObjectModel
    }
    
    fileprivate func createNewNSPersistentStoreCoordinator() -> NSPersistentStoreCoordinator {
        
        self.migrateStoreLocationIfNeeded()
        return self.createNewNSPersistentStoreCoordinator(self.storeURL(self.modelName))
    }
    
    fileprivate func createNewNSPersistentStoreCoordinator(_ storeURL: URL?) -> NSPersistentStoreCoordinator {
        
        let managedObjectModel = self.managedObjectModel
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let options = self.persistentStoreCoordinatorOptions()
        let storeType = self.persistentStoreType()
        
        do {
            
            try persistentStoreCoordinator.addPersistentStore(ofType: storeType, configurationName: nil, at: storeURL, options: options)
        }
        catch let error as NSError {
            
            /*
            Replace this implementation with code to handle the error appropriately.
            
            abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            
            Typical reasons for an error here include:
            * The persistent store is not accessible;
            * The schema for the persistent store is incompatible with current managed object model.
            Check the error message to determine what the actual problem was.
            
            
            If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
            
            If you encounter schema incompatibility errors during development, you can reduce their frequency by:
            * Simply deleting the existing store:
            [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
            
            * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
            [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
            
            Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
            
            */
            
            print("Unresolved error \(error), \(error.userInfo)");
            
            print("Deleting the DB");
            self.deleteDB()
            
            self.setupDB(self.modelName)
            return self.createNewNSPersistentStoreCoordinator()
        }
        
        return persistentStoreCoordinator
    }
    
    fileprivate func migrateStoreLocationIfNeeded() {
        
        //nothing to do here for now
    }
    
    //MARK: Save
    
    public final func saveChanges() throws {
        
        try self.performBlockAndWait(context: self.backgroundManagedObjectContext, block: { (context) -> Void in
            
            try context.save()
        })
        
        try self.performBlockAndWait(context: self.mainManagedObjectContext, block: { (context) -> Void in
            
            try context.save()
        })
        
        try self.performBlockAndWait(context: self.rootManagedObjectContext, block: { (context) -> Void in
            
            try context.save()
        })
    }
    
    //MARK: DB Management -
    
    open func setupDB(_ modelName: String) {
        
        //subclasses can override this and perform aditional actions
    }
    
    public final func deleteDB() {
        
        self.willDeleteDB()
        
        //FIXME: implement this
        
        //reset the core data stack
        self.resetCoreDataStack()
        
        //remove the shared instance
        Static.sharedInstances[self.modelName] = nil
        
        self.didDeleteDB()
    }
    
    open func willDeleteDB() {
        
        //subclasses can override this and perform aditional actions
    }
    
    open func didDeleteDB() {
        
        //subclasses can override this and perform aditional actions
    }
    
    public final class func deleteAllDBs() throws {
        
        try self.willDeleteAllDBs()
        
        //reset all stacks
        Array(self.Static.sharedInstances.values).forEach { (instance: CoreDataStack) -> Void in
            
            instance.resetCoreDataStack()
        }
        
        //remove all instances
        self.Static.sharedInstances = Dictionary<String, CoreDataStack>()
        
        try self.didDeleteAllDBs()
    }
    
    open class func willDeleteAllDBs() throws {
        
        //subclasses can override this and perform aditional actions
        //subclasses can throw an error to prevent the deletion
    }
    
    open class func didDeleteAllDBs() throws {
        
        //subclasses can override this and perform aditional actions
        //subclasses can thrown an error related to post deletion - this error has no effect on the deletion process - it is propagated up to the caller
    }
    
    public final func resetCoreDataStack() {
        
        _managedObjectModel = nil
        _persistentStoreCoordinator = nil
        
        _rootManagedObjectContext = nil
        _mainManagedObjectContext = nil
        _backgroundManagedObjectContext = nil
    }
    
    //MARK: Configuration Methods
    
    open func storeURL(_ modelName: String) -> URL? {
        
        return nil
    }
    
    open func persistentStoreCoordinatorOptions() -> [String: Bool] {
        
        let exception: () throws -> [String: Bool] = {
            
            throw CoreDataDAOError.general("You must register a valid CoreDataStack subclass and override \(#function) for model \(self.modelName)")
        }
        
        return try! exception()
    }
    
    open func persistentStoreType() -> String {
        
        let exception: () throws -> String = {
            
            throw CoreDataDAOError.general("You must register a valid CoreDataStack subclass and override \(#function) for model \(self.modelName)")
        }
        
        return try! exception()
    }
}

extension NSManagedObjectContext {
    
    public func performBlock(_ block: @escaping () throws -> Void, errorHandler: @escaping (_ error: Error) -> Void)  {
        
        self.perform { () -> Void in
            
            do {
                
                try block()
            }
            catch {
                
                errorHandler(error)
            }
        }
    }
    
    public func performBlockAndWait(block: @escaping () throws -> Void) rethrows {
        
        func impl(_ context: NSManagedObjectContext, block: @escaping () throws -> Void, throwBlock:((_ error: Error) throws -> ())) rethrows {
            
            var error: Error? = nil
            
            context.performAndWait { () -> Void in
                
                do {
                    
                    try block()
                }
                catch let e {
                    
                    error = e
                }
            }
            
            if let error = error {
                
                try throwBlock(error)
            }
        }
        
        try impl(self, block: block, throwBlock: { throw $0 })
    }
}
