//
//  EntityObserver.swift
//  CoreDataDAO
//
//  Created by Milen Halachev on 1/25/16.
//  Copyright © 2016 KoCMoHaBTa. All rights reserved.
//

import Foundation
import CoreData

public enum ContextStateChange {
    
    case willSave
    case didSave
    case didChange
}

public enum EntityStateChange {
    
    case inserted
    case updated
    case deleted
    case any
}

open class EntityObserver<E:NSManagedObject> {
    
    fileprivate let entityType: E.Type
    fileprivate let context: NSManagedObjectContext?
    fileprivate let queue: OperationQueue = OperationQueue()
    
    public init(entityType: E.Type, context: NSManagedObjectContext? = nil) {
        
        self.entityType = entityType
        self.context = context
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    open func observe(_ contextStateChange: ContextStateChange = .didSave, entityStateChange: EntityStateChange = .any, filter: ((_ entity: E) -> Bool)? = nil, changes: @escaping (_ inserted: [E], _ updated: [E], _ deleted: [E]) -> Void) -> EntityObserver<E> {
    
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: contextStateChange.notificationName), object: self.context, queue: self.queue) { [weak self] (notification) -> Void in
            
            guard let weakSelf = self else {
                
                return
            }
            
            let (inserted, updated, deleted) = weakSelf.expandNotification(notification, entityStateChange: entityStateChange)
            
            if let (inserted, updated, deleted) = weakSelf.match(inserted, updated: updated, deleted: deleted, entityStateChange: entityStateChange, filter: filter) {
                
                guard let context = notification.object as? NSManagedObjectContext ?? weakSelf.context else {
                    
                    changes(inserted, updated, deleted)
                    
                    return
                }
                
                context.perform({ () -> Void in
                    
                    changes(inserted, updated, deleted)
                })
            }
        }
        
        return self
    }
    
    fileprivate func expandNotification(_ notification: Notification, entityStateChange: EntityStateChange) -> (inserted: [E], updated: [E], deleted: [E]) {
        
        func transform(_ object: AnyObject?) -> [E] {
            
            let collection = object as? Set<NSManagedObject>
            let filtered = collection?.filter({ $0 is E }).map({ $0 as! E })
            
            return filtered ?? []
        }
        
        var inserted = [E]()
        var updated = [E]()
        var deleted = [E]()
        
        switch entityStateChange {
            
            case .inserted: inserted = transform((notification as NSNotification).userInfo?[NSInsertedObjectsKey] as AnyObject?)
            case .updated: updated = transform((notification as NSNotification).userInfo?[NSUpdatedObjectsKey] as AnyObject?)
            case .deleted: deleted = transform((notification as NSNotification).userInfo?[NSDeletedObjectsKey] as AnyObject?)
            
            default:
                inserted = transform((notification as NSNotification).userInfo?[NSInsertedObjectsKey] as AnyObject?)
                updated = transform((notification as NSNotification).userInfo?[NSUpdatedObjectsKey] as AnyObject?)
                deleted = transform((notification as NSNotification).userInfo?[NSDeletedObjectsKey] as AnyObject?)
        }
        
        return(inserted, updated, deleted)
    }
    
    fileprivate func match(_ inserted: [E], updated: [E], deleted: [E], entityStateChange: EntityStateChange, filter: ((_ entity: E) -> Bool)?) -> (inserted: [E], updated: [E], deleted: [E])? {
        
        var inserted = inserted
        var updated = updated
        var deleted = deleted
        
        switch entityStateChange {
            
            case .inserted:
                
                if let filter = filter {
                    
                    inserted = inserted.filter(filter)
                }
                
                if inserted.isEmpty {
                    
                    return nil
                }
            
            case .updated:
            
                if let filter = filter {
                    
                    updated = updated.filter(filter)
                }
                
                if updated.isEmpty {
                    
                    return nil
                }
            
            case .deleted:
            
                if let filter = filter {
                    
                    deleted = deleted.filter(filter)
                }
                
                if deleted.isEmpty {
                    
                    return nil
                }
            
            default:
                
                if let filter = filter {
                    
                    inserted = inserted.filter(filter)
                    updated = updated.filter(filter)
                    deleted = deleted.filter(filter)
                }
                
                if inserted.isEmpty && updated.isEmpty && deleted.isEmpty {
                    
                    return nil
                }
        }
        
        return(inserted, updated, deleted)
    }
}

extension ContextStateChange {
    
    //TODO: Convert this to Notification.Name
    fileprivate var notificationName: String {
        
        switch self {
            
        case .willSave: return NSNotification.Name.NSManagedObjectContextWillSave.rawValue
        case .didSave: return NSNotification.Name.NSManagedObjectContextDidSave.rawValue
        case .didChange: return NSNotification.Name.NSManagedObjectContextObjectsDidChange.rawValue
        }
    }
}

public func EntityObserverCreate<E: NSManagedObject>(_ entityType: E.Type, context: NSManagedObjectContext? = nil) -> EntityObserver<E> {
    
    return EntityObserver(entityType: entityType, context: context)
}
