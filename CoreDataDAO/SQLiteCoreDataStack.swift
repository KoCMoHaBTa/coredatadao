//
//  SQLiteCoreDataStack.swift
//  CoreDataDAO
//
//  Created by Milen Halachev on 4/14/15.
//  Copyright (c) 2015 KoCMoHaBTa. All rights reserved.
//

import UIKit
import CoreData

open class SQLiteCoreDataStack: CoreDataStack {
    
    //MARK: - Directories
    
    public final class func storeDirectoryWithName(_ name: String) -> URL {
        
        let coreDataDirectory = self.coreDataDirectory()
        let storeDirectory = coreDataDirectory.appendingPathComponent(name, isDirectory: true)
        
        let _ = try? FileManager.default.createDirectory(at: storeDirectory, withIntermediateDirectories: true, attributes: nil)
        
        return storeDirectory
    }
    
    fileprivate final class func storeFileWithName(_ name: String) -> URL {
        
        let storeDirectory = self.storeDirectoryWithName(name)
        let storeExtension = self.storeFileExtension()
        let storeFile = storeDirectory.appendingPathComponent(name, isDirectory: false).appendingPathExtension(storeExtension)
        return storeFile
    }
    
    //MARK: - DB Management
    
    open override func setupDB(_ modelName: String) {
        
        let _ = type(of: self).storeFileWithName(modelName)
    }
    
    open override func didDeleteDB() {
        
        //delete the store from the file system
        let coreDataDirectory: URL = type(of: self).coreDataDirectory()
        let _ = try? FileManager.default.removeItem(at: coreDataDirectory)
    }
    
    
    
    open override class func didDeleteAllDBs() throws {
        
        //delete the store from the file system
        let coreDataDirectory: URL = self.coreDataDirectory()
        try FileManager.default.removeItem(at: coreDataDirectory)
    }
    
    //MARK: - SQLiteCoreDataStack Configuration Methods
    
    open class func coreDataDirectory() -> URL {
        
        let libraryDirectory: URL = (FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask) ).first!
        let coreDataDirectory = libraryDirectory.appendingPathComponent("CoreDataStores", isDirectory: true)
        let _ = try? FileManager.default.createDirectory(at: coreDataDirectory, withIntermediateDirectories: true, attributes: nil)
        
        return coreDataDirectory
    }
    
    open class func storeFileExtension() -> String {
        
        return "sqlite"
    }
    
    //MARK: - CoreDataStack Configuration Methods
    
    open override func storeURL(_ modelName: String) -> URL? {
        
        return type(of: self).storeFileWithName(modelName)
    }
   
    open override func persistentStoreCoordinatorOptions() -> [String: Bool] {
        
        return [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
    }
    
    open override func persistentStoreType() -> String {
        
        return NSSQLiteStoreType
    }
}
