//
//  InMemoryCoreDataStack.swift
//  CoreDataDAO
//
//  Created by Milen Halachev on 4/14/15.
//  Copyright (c) 2015 KoCMoHaBTa. All rights reserved.
//

import UIKit
import CoreData

open class InMemoryCoreDataStack: CoreDataStack {
   
    open override func persistentStoreCoordinatorOptions() -> [String: Bool] {
        
        return [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
    }
    
    open override func persistentStoreType() -> String {
        
        return NSInMemoryStoreType
    }
}
